<?php

namespace App\Services;

class UserService
{
    public function ifHasModifyRole(&$users)
    {
        $usersNew = array();
        foreach($users as $key => $user){
            $usersNew[$key]['user'] = $user;
            if(FALSE !== array_search('ROLE_MODIFY', $user->getRoles())){
                $usersNew[$key]['modify'] = true;
            }else{
                $usersNew[$key]['modify'] = false;
            }
        }
        return $usersNew;
    }

    public function getFormErrors($form)
    {
        $errors = array();
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
               $errors[$child->getName()] = (String) $form[$child->getName()]->getErrors();
            }
        }
        return $errors;
    }

}
