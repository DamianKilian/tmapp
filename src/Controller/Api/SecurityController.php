<?php

namespace App\Controller\Api;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api")
 */
class SecurityController extends AbstractController
{

    /**
     * @Route("/login", methods={"POST"}, name="api_login")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
		$params = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->findOneBy(['email' => $params['email']]);
		if($user && $encoder->isPasswordValid($user, $params['password'])){
			$token = bin2hex(openssl_random_pseudo_bytes(30));
			$user->setApiToken($token);
			$em->flush();
			return new JsonResponse([
				'token' => $token,
			]);
		}
		return new JsonResponse([
			'err' => 'niepowodzenie w logowaniu'
		]);
    }

    /**
     * @Route("/logout", methods={"POST"}, name="api_logout")
     */
    public function logout(Request $request): Response
    {
		$params = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->findOneBy(['apiToken' => $params['token']]);
        if ($user) {
            $user->setApiToken(null);
            $em->flush();
			return new JsonResponse([
				'data' => 'Wylogowano'
			]);
        }
		return new JsonResponse([
			'err' => 'błąd'
		]);
    }
}
