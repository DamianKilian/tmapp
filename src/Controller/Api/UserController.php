<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api/users")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="api_users")
     */
    public function index(){
		$em = $this->getDoctrine()->getManager();
		return new JsonResponse([
			'users' => $em->getRepository(User::class)->createQueryBuilder('u')->select('u.email')->getQuery()->execute(),
		]);
	}

    /**
     * @Route("/create", methods={"POST"}, name="api_users_create")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder){
		$params = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();
		if($em->getRepository(User::class)->findOneBy(['email' => $params['email']])){
			return new JsonResponse([
				'err' => 'Podany użytkownik juz istnieje'
			]);
		}
		$user = new User();
		$user->setEmail($params['email']);
		$user->setPassword($encoder->encodePassword($user, $params['password']));
		$em->persist($user);
		$em->flush();
		return new JsonResponse([
			'success' => 'Użytkownik "'.$params['email'].'" utworzony',
		]);
	}

    /**
     * @Route("/read/{id}", methods={"GET"}, name="api_users_read")
     */
    public function read(Request $request, $id){
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->find($id);
		if(!$user){
			return new JsonResponse([
				'err' => 'Podany użytkownik nie istnieje'
			]);
		}
		return new JsonResponse([
			'id' => $user->getId(),
			'email' => $user->getEmail(),
		]);
	}

    /**
     * @Route("/update/{id}", methods={"PUT"}, name="api_users_update")
     */
    public function update(Request $request, $id){
		$params = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->find($id);
		if(!$user){
			return new JsonResponse([
				'err' => 'Podany użytkownik nie istnieje'
			]);
		}
		$oldEmail = $user->getEmail();
		$user->setEmail($params['email']);
		$em->flush();
		return new JsonResponse([
			'old_email' => $oldEmail,
			'new_email' => $params['email'],
		]);
	}

    /**
     * @Route("/delete/{id}", methods={"DELETE"}, name="api_users_delete")
     */
    public function delete(Request $request, $id){
		$params = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository(User::class)->find($id);
		if(!$user){
			return new JsonResponse([
				'err' => 'Podany użytkownik nie istnieje'
			]);
		}
		$email = $user->getEmail();
		$em->remove($user);
		$em->flush();
		return new JsonResponse([
			'success' => 'użytkownik usunięty',
			'email' => $email,
		]);
	}    
}
