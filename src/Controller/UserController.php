<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\EditFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\UserService;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->render('user/index.html.twig', []);
    }

    /**
     * @Route("/authorization", name="authorization")
     * @Method("GET")
     */
    public function authorizationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        $userService = new UserService;
        return $this->render('user/authorization.html.twig', [
            'users' => $userService->ifHasModifyRole($users),
        ]);
    }

    /**
     * @Route("/authorize", requirements={"id": "\d+"}, name="authorize")
     * @Method("POST")
     */
    public function authorizeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($_POST['userid']);
        $roles = $user->getRoles();
        if('true' === $_POST['modify']){
            $roles[] = 'ROLE_MODIFY';
            $user->setRoles($roles);
        }else{
            $modifyKey = array_search('ROLE_MODIFY', $roles);
            if(false !== $modifyKey){
                unset($roles[$modifyKey]);
                $user->setRoles($roles);
            }
        }
        $em->flush();
        return new JsonResponse(array('modify' => $_POST['modify']));
    }

    /**
     * @Route("/register", name="register")
     * @Method("POST")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()){
                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return new JsonResponse(array('valid' => $user->getEmail().', jesteś zarejestrowany'));
            }else{
                $userService = new UserService;
                return new JsonResponse(array('valid' => 0, 'errors' => $userService->getFormErrors($form)));
            }

        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edituser", requirements={"id": "\d+"}, name="edituser")
     * @Method({"POST"})
     */
    public function edituserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if(!$this->isGranted('ROLE_MODIFY')){
            return new JsonResponse(array('valid' => 0, 'errors' => array('_globalError' => 'Nie masz uprawnień do modyfikacji')));
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $form = $this->createForm(EditFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()){
                $oldPassword = $form->get('oldPassword')->getData();
                if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                    $newEncodedPassword = $passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData());
                    $user->setPassword($newEncodedPassword);
                    $em->flush();
                    return new JsonResponse(array('valid' => 'Hasło zmienione', 'errors' => array()));
                }else{
                    return new JsonResponse(array('valid' => 0, 'errors' => array('oldPassword' => 'Nieprawidłowe hasło')));
                }
            }else{
                $userService = new UserService;
                return new JsonResponse(array('valid' => 0, 'errors' => $userService->getFormErrors($form)));
            }
        }

    }

    /**
     * @Route("/account", name="account")
     * @Method("GET")
     */
    public function accountAction(Request $request)
    {
        $user = $this->getUser();
        $editForm = $this->createForm(EditFormType::class, $user);
        $removeForm = $this->createFormBuilder($user)->getForm();
        return $this->render('user/account.html.twig', [
            'role_modify' => $this->isGranted('ROLE_MODIFY'),
            'user' => $this->getUser(),
            'editForm' => $editForm->createView(),
            'removeForm' => $removeForm->createView(),
        ]);
    }

    /**
     * @Route("/deleteuser", name="deleteuser")
     * @Method({"POST"})
     */
    public function deleteuserAction(Request $request, SessionInterface $session, TokenStorageInterface $tokenStorage)
    {
        if(!$this->isGranted('ROLE_MODIFY')){
            return new JsonResponse(array('valid' => 0, 'errors' => array('_globalError' => 'Nie masz uprawnień do modyfikacji')));
        }
        $user = $this->getUser();
        $removeForm = $this->createFormBuilder($user)->getForm();
        $removeForm->handleRequest($request);
        if($removeForm->isSubmitted() && $removeForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }
        $tokenStorage->setToken(null);
        $session->invalidate();
        return new JsonResponse(array('valid' => 'Twoje konto zostało usunięte', 'errors' => array()));
    }

}
