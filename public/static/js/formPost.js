"use strict";

window.appFormPost = {
    form: $('.postForm form'),
    init: function(){
        if(!appFormPost.form.length){
            return;
        }
        appFormPost.events();
    },
    events: function(){
        appFormPost.form.on('submit', function(e){
            e.preventDefault();
            var form = $(this);
            var error = form.closest('.postForm').find('.error');
            error.html('');
            $.ajax({
                method: 'POST',
                url: form.attr('action')? form.attr('action') : window.location.href,
                dataType: 'JSON',
                data: form.serialize()
            })
            .done(function( msg ) {
                if(0 !== msg.valid){
                    error.filter('.valid').html(msg.valid);
                }else{
                    $.each(msg.errors, function(index, value){
                        error.filter('.'+index+'Error').html(value);
                    });
                }
            });
        });
    }
};

window.appFormPost.init();
