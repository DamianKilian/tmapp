"use strict";

window.appUser = {
    roleModify: $('.roleModify'),
    init: function(){
        appUser.events();
    },
    events: function(){
        if(appUser.roleModify.length){
            appUser.roleModify.on('click', function(){
                var btn = $(this);
                var loader = btn.closest('li').find('.modifyloader');
                loader.addClass('loader');
                $.ajax({
                    method: 'POST',
                    url: '/authorize',
                    dataType: 'JSON',
                    data: {
                        userid: btn.data('userid'),
                        modify: btn.hasClass('add')
                    }
                })
                .done(function( msg ) {
                    loader.removeClass('loader');
                    if('true' === msg.modify){
                        btn.removeClass('btn-primary add').addClass('btn-danger remove').html('Usuń ROLE_MODIFY');
                    }else if('false' === msg.modify){
                        btn.addClass('btn-primary add').removeClass('btn-danger remove').html('Dodaj ROLE_MODIFY');
                    }
                });
            });
        }
    }
};

window.appUser.init();
